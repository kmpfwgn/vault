path "auth/*" {
    capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

path "sys/auth/*" {
    capabilities = ["create", "update", "delete", "sudo"]
}

path "sys/auth" {
    capabilities = ["read"]
}

path "sys/policies/acl" {
    capabilities = ["list"]
}

path "sys/policies/acl/*" {
    capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

path "secret/*" {
    capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

path "kv/*" {
    capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

path "kv-v2/*" {
    capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

path "sys/mounts/*" {
    capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

path "sys/mounts" {
    capabilities = ["read"]
}

path "sys/healt" {
    capabilities = ["read", "sudo"]
}
